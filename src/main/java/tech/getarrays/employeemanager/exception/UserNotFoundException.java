package tech.getarrays.employeemanager.exception;

/**
 * Exception para cuando no se encuentra un usuario
 * @author Cristian PLazas
 */
public class UserNotFoundException extends RuntimeException {

    /**
     * Constructor de la excepcion
     * @param message Mensaje de la excepcion
     */
    public UserNotFoundException(String message) {
        super(message);
    }
}
