package tech.getarrays.employeemanager.service;

import java.util.List;

import tech.getarrays.employeemanager.model.Employee;

public interface EmployeeService {
    List<Employee> findAllEmployees();
    Employee findEmployeeById(Long id);
    Employee addEmployee(Employee employee);
    Employee updateEmployee(Employee employee);
    void deleteEmployee(Long id);
}