package tech.getarrays.employeemanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.getarrays.employeemanager.exception.UserNotFoundException;
import tech.getarrays.employeemanager.model.Employee;
import tech.getarrays.employeemanager.repository.EmployeeRepo;
import tech.getarrays.employeemanager.service.EmployeeService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class EmployeeServiceImpl  implements EmployeeService {
    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    /**
     * Metodo que retorna todos los funcionarios
     * @return Lista de funcionarios
     */
    public Employee addEmployee(Employee employee) {
        employee.setEmployeeCode(UUID.randomUUID().toString());
        return employeeRepo.save(employee);
    }

    /**
     * Metodo que retorna un funcionario por id
     * @param id Id del funcionario
     * @return Funcionario
     */
    public List<Employee> findAllEmployees() {
        return employeeRepo.findAll();
    }

    /**
     * Metodo que actualiza un funcionario
     * @param id Id del funcionario
     * @return Funcionario
     */
    public Employee updateEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    /**
     * Metodo que retorna un funcionario por id
     * @param id Id del funcionario
     * @return Funcionario
     */
    public Employee findEmployeeById(Long id) {
        return employeeRepo.findEmployeeById(id)
                .orElseThrow(() -> new UserNotFoundException("User by id " + id + " was not found"));
    }

    /**
     * Metodo que borra un funcionario por id
     * @param id Id del funcionario
     *  @return Funcionario
     */
    public void deleteEmployee(Long id){
        employeeRepo.deleteEmployeeById(id);
    }
}
